FROM kiasaki/alpine-golang

WORKDIR /gopath/src/go48
ADD . /gopath/src/go48/
RUN go get && go build

CMD /gopath/src/go48/go48 -np

