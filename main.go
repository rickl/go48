// vim: noexpandtab:ts=4:sw=4

package main

import (
	"flag"
	"fmt"
	"github.com/davecheney/profile"
	"math"
	"math/rand"
	"os"
	//"sync"
	"time"
)

const (
	EMPTY_VALUE = 128
	MERGE_SCORE = 256
	BOARD_SIZE = 4
	LOSE = -10000000

	// must align with Row type
	MAX_ROW_VALUE = math.MaxUint16 + 1
)

type (
	Row uint16
	Board uint64
	Dir int
	RowScoreTable [MAX_ROW_VALUE]int
	MoveLookupTable [MAX_ROW_VALUE]Row
	SearchScore int32
)

func makeSearchScore(depth int, score int) SearchScore {
	return SearchScore(uint32(depth) << 24 | uint32(score) & 0x80ffffff)
}

func (s SearchScore) depth() int32 {
	return int32(uint32(s & 0x7f000000) >> 24)
}

func (s SearchScore) score() int32 {
	if s < 0 {
		return int32(s | 0x7f000000)
	} else {
		return int32(s & 0x00ffffff)
	}
}

const (
	MAX_CACHE_STEPS = 64
	SEARCH_CACHE_ENTRY_PER_STEP = 65536*2
)

const (
	UP Dir = 1 + iota
	LEFT
	RIGHT
	DOWN
)

var allDirs = [4]Dir{UP, LEFT, RIGHT, DOWN}

var (
	rowScoreTable RowScoreTable
	moveRowLeftTable MoveLookupTable
	moveRowRightTable MoveLookupTable

	searchScoreCache = make(map[Board]SearchScore)
	searchHit [30]int
	searchMiss [30]int
	boardEvaluated int
)

//var boardCacheLock sync.RWMutex
//var searchCacheLock sync.RWMutex

//var rowScoreMultipliers = [4]int{3, 2, 1, 1}
const (
	ROW1_SCORE_MULTIPLIER = 5
	ROW2_SCORE_MULTIPLIER = 2
)

var (
	quiet = flag.Bool("q", false, "Quiet mode: only output game end board")
	debug = flag.Bool("d", false, "Enable debug logs")
	profiling = flag.Bool("prof", false, "Enable profiling")
	seed = flag.Int("s", 0, "Specify random seed; 0 will use system timestamp")
	numberToStartWith = flag.Int("sw", 0, "Place a number in start board; for benchmarking use only")
	boardToStartWith = flag.Uint64("swb", 0, "Start with specified board in hex; for benchmarking use only")
)

func main() {
	flag.Parse()
	buildLookupTables()

	if *profiling {
		cfg := profile.Config{
			CPUProfile:     true,
			ProfilePath:    ".",  // store profiles in current directory
			NoShutdownHook: true, // do not hook SIGINT
		}
		defer profile.Start(&cfg).Stop()
	}

	if *seed == 0 {
		rand.Seed(time.Now().UTC().UnixNano())
	} else {
		rand.Seed(int64(*seed))
	}
	runGame()
	if *debug {
		//fmt.Fprintf(os.Stderr, "board cache hit %d, miss %d, hit rate %.2f%%\n",
		//	boardHit, boardMiss, float64(boardHit*100)/float64(boardHit+boardMiss))
		totalHits := 0
		totalMisses := 0
		for i, n := range searchHit {
			if n > 0 {
				fmt.Fprintf(os.Stderr, "search hit @ depth %d: %d\n", i, n)
			}
			totalHits += n
		}
		for i, n := range searchMiss {
			if n > 0 {
				fmt.Fprintf(os.Stderr, "search miss @ depth %d: %d\n", i, n)
			}
			totalMisses += n
		}
		totalSearches := float64(totalHits + totalMisses)
		fmt.Fprintf(os.Stderr, "search cache hit %d, miss %d, rate %.2f%%\n",
			totalHits, totalMisses, float64(totalHits*100)/float64(totalSearches));
		fmt.Fprintf(os.Stderr, "board evaluated %d\n", boardEvaluated);
	}
}

func (row Row) String() string {
	numbers := unpackRow(row)
	res := ""
	for _, n := range numbers {
		res += fmt.Sprintf("|%d|", bitToNum(n))
	}
	return res
}

func (b Board) String() string {
	rows := unpackBoard(b)
	res := ""
	for _, r := range rows {
		res += Row(r).String() + "\n"
	}
	return res
}

func buildLookupTables() {
	for i := 0; i < MAX_ROW_VALUE; i++ {
		rowScoreTable[i] = evaluateRow(Row(i))
		moveRowLeftTable[i] = moveRowLeft(Row(i))
		moveRowRightTable[i] = moveRowRight(Row(i))
	}
}

func resetCacheTables() {
	searchScoreCache = make(map[Board]SearchScore, SEARCH_CACHE_ENTRY_PER_STEP * MAX_CACHE_STEPS)
}

func evaluatePair(a uint, b uint) int {
	if a == 0 {
		return EMPTY_VALUE
	}
	/*
	var penalty int
	if a <= 4 {
		penalty -= 1000 * (4 / int(a))
	}
	if b <= 4 {
		penalty -= 1000 * (4 / int(a))
	}
	*/
	aScore := int(math.Pow(float64(a), 1.3))
	switch {
	case a == b:
		//return aScore + int(a)/2
		return aScore * 2 //+ penalty
	case a/2 == b:
		//return aScore + int(a)/4
		return aScore + aScore / 2 //+ penalty
	case a > b:
		return aScore //+ penalty
	default:
		bScore := int(math.Pow(float64(b), 1.7))
		return aScore - bScore //+ penalty
		/*
		switch {
		case b <= 64:
			return aScore - bScore*2
		case b <= 512:
			return aScore - bScore*6
		default:
			return aScore - bScore*8
		}
		*/
	}
}

func bitToNum(n uint) uint {
	if n == 0 {
		return 0
	}
	return 1 << n
}

func numToBit(n uint) uint {
	if n == 0 {
		return 0
	}
	return uint(math.Log2(float64(n)))
}

func unpackRow(row Row) []uint {
	rowVal := uint(row)
	// With C/C++ we may use SIMD _mm_extract_epi8/16
	return []uint{rowVal >> 12 & 0xf, rowVal >> 8 & 0xf, rowVal >> 4 & 0xf, rowVal & 0xf}
}

func unpackBoard(board Board) []uint {
	bitmap := uint64(board)
	return []uint{uint(bitmap >> 48 & 0xffff), uint(bitmap >> 32 & 0xffff), uint(bitmap >> 16 & 0xffff), uint(bitmap & 0xffff)}
}

func unpackBoardToArray(board Board) [4]uint {
	bitmap := uint64(board)
	return [4]uint{uint(bitmap >> 48 & 0xffff), uint(bitmap >> 32 & 0xffff), uint(bitmap >> 16 & 0xffff), uint(bitmap & 0xffff)}
}

func evaluateRow(row Row) int {
	numbers := append(unpackRow(row), 0)
	score := 0
	for i := 0; i < len(numbers)-1; i++ {
		score += evaluatePair(bitToNum(numbers[i]), bitToNum(numbers[i+1]))
	}
	if row != moveRowLeft(row) {
		score += MERGE_SCORE
	}
	return score
}

func maxint(a int, b int) int {
	if a >= b {
		return a
	}
	return b
}

func evaluateBoardFast(board Board) int {
	b := uint64(board)
	row1, row2, row3, row4 := Row(b >> 48), Row(b >> 32), Row(b >> 16), Row(b)
	score := rowScoreTable[row1] * ROW1_SCORE_MULTIPLIER +
		rowScoreTable[row2] * ROW2_SCORE_MULTIPLIER +
		rowScoreTable[row3] +
		rowScoreTable[row4]
	return score
}

func evaluateBoardAux(board Board) int {
	b := uint64(board)
	row1, row2, row3, row4 := Row(b >> 48), Row(b >> 32), Row(b >> 16), Row(b)
	return rowScoreTable[row1] +
		rowScoreTable[row2] +
		rowScoreTable[row3] +
		rowScoreTable[row4]
}

func evaluateBoard(board Board) int {
	/*
	transposed := transposePacked(board)
	var score int
	if board > transposed {
		score = evaluateBoardFast(board) + evaluateBoardAux(transposed)
	} else {
		score = evaluateBoardAux(board) + evaluateBoardFast(transposed)
	}
	*/
	boardEvaluated++
	score := evaluateBoardFast(board) + evaluateBoardFast(transposePacked(board))

	/*
	score := 0
	rows := unpackBoardToArray(board)
	score += rowScoreTable[rows[0]] * 3
	score += rowScoreTable[rows[1]] * 2
	score += rowScoreTable[rows[2]]
	score += rowScoreTable[rows[3]]
	*/
	//leftMovable, rightMovable, upMovable, downMovable := 0, 0, 0, 0
	//for i, row := range rows {
	//	score += rowScoreTable[row] * rowScoreMultipliers[i]
		/*
		if moveRowLeftTable[row] != Row(row) {
			leftMovable++
		}
		if moveRowRightTable[row] != Row(row) {
			rightMovable++
		}
		*/
	//}

	/*
	rows = unpackBoardToArray(transposePacked(board))
	score += rowScoreTable[rows[0]] * 3
	score += rowScoreTable[rows[1]] * 2
	score += rowScoreTable[rows[2]]
	score += rowScoreTable[rows[3]]
	*/

	//for i, col := range rows {
	//	score += rowScoreTable[col] * rowScoreMultipliers[i]
		/*
		if moveRowLeftTable[col] != Row(col) {
			upMovable++
		}
		if moveRowRightTable[col] != Row(col) {
			downMovable++
		}
		*/
	//}

	/*
	empties := numEmptyCells(board) + maxint(leftMovable, maxint(rightMovable, maxint(upMovable, downMovable)))
	penalty := 0
	switch empties {
	case 0:
		penalty = LOSE
	// empties can't be 1
	case 2:
		penalty = LOSE / 2
	case 3:
		penalty = -20000
	}
	score += penalty
	if empties < 2 {
		fmt.Printf("%d %d %v", empties, score, board)
	}
	*/

	return score
}

func transpose(rows []uint) []uint {
	result := []uint{
		(rows[0] & 0xf000) | (rows[1]&0xf000)>>4 | (rows[2]&0xf000)>>8 | (rows[3]&0xf000)>>12,
		(rows[0]&0x0f00)<<4 | (rows[1] & 0x0f00) | (rows[2]&0x0f00)>>4 | (rows[3]&0x0f00)>>8,
		(rows[0]&0x00f0)<<8 | (rows[1]&0x00f0)<<4 | (rows[2] & 0x00f0) | (rows[3]&0x00f0)>>4,
		(rows[0]&0x000f)<<12 | (rows[1]&0x000f)<<8 | (rows[2]&0x000f)<<4 | (rows[3] & 0x000f),
	}
	return result
}

func transposePacked(board Board) Board {
	b := uint64(board)
	ret := (b & 0xf000f000f000f000) * 0x1001001001 & 0xffff000000000000 |
	       (b & 0x0f000f000f000f00) * 0x1001001001 & 0x0ffff00000000000 >> 12 |
		   (b & 0x00f000f000f000f0) * 0x1001001001 & 0x00ffff0000000000 >> 24 |
		   (b & 0x000f000f000f000f) * 0x1001001001 & 0x000ffff000000000 >> 36;
	return Board(ret)
}

/*
 * OK this is dirty...
 */
func transposeInPlace(rows [4]uint) [4]uint {
	rows[0], rows[1], rows[2], rows[3] =
		(rows[0]&0xf000)|(rows[1]&0xf000)>>4|(rows[2]&0xf000)>>8|(rows[3]&0xf000)>>12,
		(rows[0]&0x0f00)<<4|(rows[1]&0x0f00)|(rows[2]&0x0f00)>>4|(rows[3]&0x0f00)>>8,
		(rows[0]&0x00f0)<<8|(rows[1]&0x00f0)<<4|(rows[2]&0x00f0)|(rows[3]&0x00f0)>>4,
		(rows[0]&0x000f)<<12|(rows[1]&0x000f)<<8|(rows[2]&0x000f)<<4|(rows[3]&0x000f)
	return rows
}

func moveRowLeft(row Row) Row {
	numbers := unpackRow(row)
	result := make([]uint, 0, 4)
	for _, n := range numbers {
		if n != 0 {
			result = append(result, uint(n))
		}
	}

	for i := range result {
		if i+1 < len(result) && result[i] == result[i+1] {
			// by +1 we're doubling its represented value
			result[i]++

			// and merge only once
			result = append(result[:i+1], result[i+2:]...)
			break
		}
	}
	for len(result) < 4 {
		result = append(result, 0)
	}
	return Row(result[0]<<12 | result[1]<<8 | result[2]<<4 | result[3])
}

func reverseRow(row Row) Row {
	val := uint(row)
	return Row((val&0xf)<<12 | (val&0xf0)<<4 | (val&0xf00)>>4 | (val&0xf000)>>12)
}

func moveRowRight(row Row) Row {
	return reverseRow(moveRowLeft(reverseRow(row)))
}

func Map(xs []uint, f func(uint) uint) []uint {
	result := make([]uint, len(xs))
	for i, n := range xs {
		result[i] = f(n)
	}
	return result
}

// OK so this is even dirtier...
func MapInPlace(xs []uint, f func(uint) uint) []uint {
	for i, n := range xs {
		xs[i] = f(n)
	}
	return xs
}

// Hmmm...
func MapInPlaceUnrolled(xs []uint, f func(uint) uint) []uint {
	xs[0] = f(xs[0])
	xs[1] = f(xs[1])
	xs[2] = f(xs[2])
	xs[3] = f(xs[3])
	return xs
}

func moveBoardFast(board Board, lookupTable *MoveLookupTable) Board {
	b := uint64(board)
	row1, row2, row3, row4 :=
		lookupTable[Row(b >> 48)],
		lookupTable[Row(b >> 32)],
		lookupTable[Row(b >> 16)],
		lookupTable[Row(b)]
	return Board(uint64(row1)<<48 | uint64(row2)<<32 | uint64(row3)<<16 | uint64(row4))
}

func moveBoard(board Board, dir Dir) Board {
	switch dir {
	case LEFT:
		return moveBoardFast(board, &moveRowLeftTable)
		/*
		rows := unpackBoardToArray(board)
		rows[0] = uint(moveRowLeftTable[rows[0]])
		rows[1] = uint(moveRowLeftTable[rows[1]])
		rows[2] = uint(moveRowLeftTable[rows[2]])
		rows[3] = uint(moveRowLeftTable[rows[3]])
		outRows = rows
		*/
		/*
		outRows = MapInPlaceUnrolled(rows, func(row uint) uint {
			return uint(moveRowLeftTable[row])
		})
		*/
	case RIGHT:
		return moveBoardFast(board, &moveRowRightTable)
		/*
		rows := unpackBoardToArray(board)
		rows[0] = uint(moveRowRightTable[rows[0]])
		rows[1] = uint(moveRowRightTable[rows[1]])
		rows[2] = uint(moveRowRightTable[rows[2]])
		rows[3] = uint(moveRowRightTable[rows[3]])
		outRows = rows
		*/
		/*
		outRows = MapInPlaceUnrolled(rows, func(row uint) uint {
			return uint(moveRowRightTable[row])
		})
		*/
	case UP:
		return transposePacked(moveBoardFast(transposePacked(board), &moveRowLeftTable))
		/*
		rows := unpackBoardToArray(transposePacked(board))
		rows[0] = uint(moveRowLeftTable[rows[0]])
		rows[1] = uint(moveRowLeftTable[rows[1]])
		rows[2] = uint(moveRowLeftTable[rows[2]])
		rows[3] = uint(moveRowLeftTable[rows[3]])
		outRows = transposeInPlace(rows)
		*/
		/*
		outRows = transposeInPlace(MapInPlaceUnrolled(transposeInPlace(rows), func(row uint) uint {
			return uint(moveRowLeftTable[row])
		}))
		*/
	case DOWN:
		fallthrough
	default:
		return transposePacked(moveBoardFast(transposePacked(board), &moveRowRightTable))
		/*
		rows := unpackBoardToArray(transposePacked(board))
		rows[0] = uint(moveRowRightTable[rows[0]])
		rows[1] = uint(moveRowRightTable[rows[1]])
		rows[2] = uint(moveRowRightTable[rows[2]])
		rows[3] = uint(moveRowRightTable[rows[3]])
		outRows = transposeInPlace(rows)
		*/
		/*
		outRows = transposeInPlace(MapInPlaceUnrolled(transposeInPlace(rows), func(row uint) uint {
			return uint(moveRowRightTable[row])
		}))
		*/
	}
	//return Board(uint64(outRows[0])<<48 | uint64(outRows[1])<<32 | uint64(outRows[2])<<16 | uint64(outRows[3]))
}

func placePiece(board Board, pos uint, n uint) Board {
	return Board(uint64(board) | (uint64(numToBit(n)) << 60 >> (pos * 4)))
}

func placePiecePacked(board Board, pos uint, packedPiece uint) Board {
	return Board(uint64(board) | (uint64(packedPiece) << 60 >> (pos * 4)))
}

func emptyCells(board Board) []uint {
	result := make([]uint, 0, 16)
	var i uint
	for i = 0; i < 16; i++ {
		if uint64(board)&((0xf<<60)>>(i*4)) == 0 {
			result = append(result, i)
		}
	}
	return result
}

func emptyCellsInPlace(board Board, result *[16]uint) int {
	count := 0
	var i uint
	for i = 0; i < 16; i++ {
		if uint64(board)&((0xf<<60)>>(i*4)) == 0 {
			result[count] = i
			count++
		}
	}
	return count
}

func numEmptyCells(board Board) uint {
	var i uint
	var empty uint
	for i = 0; i < 16; i++ {
		if uint64(board)&((0xf<<60)>>(i*4)) == 0 {
			empty++
		}
	}
	return empty
}

func numEmptyCellsFast(board Board) uint {
	bitmap := uint64(board)
	bitmap |= (bitmap & 0xcccccccccccccccc) >> 2
	bitmap |= (bitmap & 0x2222222222222222) >> 1
	bitmap &= 0x1111111111111111
	if bitmap == 0x1111111111111111 {
		return 0
	}
	bitmap += bitmap >> 32
	bitmap += bitmap >> 16
	bitmap += bitmap >> 8
	bitmap += bitmap >> 4
	return uint(16 - bitmap & 0xf)
}

func placeRandom24(board Board) Board {
	empty := emptyCells(board)
	candidiates := [2]uint{2, 4}
	return placePiece(board, empty[rand.Intn(len(empty))], candidiates[rand.Intn(len(candidiates))])
}

func createStartBoard() Board {
	if *boardToStartWith > 0 {
		return Board(*boardToStartWith)
	}
	var board Board
	set := make(map[uint]bool)
	for len(set) < 3 {
		set[uint(rand.Intn(16))] = true
	}
	for pos := range set {
		board = placePiece(board, pos, 2)
	}
	if *numberToStartWith != 0 {
		board = placePiece(board, 0, uint(*numberToStartWith))
	}
	return board
}

func availMoves(board Board) ([]Dir, []Board) {
	moves := make([]Dir, 0, 4)
	boards := make([]Board, 0, 4)
	for _, dir := range allDirs {
		newBoard := moveBoard(board, dir)
		if board != newBoard {
			moves = append(moves, dir)
			boards = append(boards, newBoard)
		}
	}
	return moves, boards
}

func availMovesInPlace(board Board, nextMoves *[4]Dir, nextBoards *[4]Board) int {
	numMoves := 0
	for _, dir := range allDirs {
		newBoard := moveBoard(board, dir)
		if board != newBoard {
			nextMoves[numMoves] = dir
			nextBoards[numMoves] = newBoard
			numMoves++
		}
	}
	return numMoves
}

func alphaAveragePlayer(board Board, depth int, steps int, extSearch bool) int {
	if depth == 0 {
		return evaluateBoard(board)
	}
	/*
	_, nextBoards := availMoves(board)
	if len(nextBoards) == 0 {
		return LOSE + steps
	}
	max := 0
	for _, nextBoard := range nextBoards {
		score := alphaAverageObstacle(nextBoard, depth-1, steps+1)
		if score > max {
			max = score
		}
	}
	if (nextBoards) == 0 {
		return LOSE + steps
	}
	max := 0
	for _, nextBoard := range nextBoards {
		score := alphaAverageObstacle(nextBoard, depth-1, steps+1)
		if score > max {
			max = score
		}
	}
	*/
	var moves [4]Dir
	var nextBoards [4]Board
	numMoves := availMovesInPlace(board, &moves, &nextBoards)
	if numMoves == 0 {
		return LOSE + steps
	}
	max := LOSE
	for i := 0; i < numMoves; i++ {
		score := alphaAverageObstacle(nextBoards[i], depth-1, steps+1, extSearch)
		if score > max {
			max = score
		}
	}
	return max
}

func alphaAverageObstacle(board Board, depth int, steps int, extSearch bool) int {
	if depth == 0 {
		return evaluateBoard(board)
	}
	//searchCacheLock.RLock()
	cache, ok := searchScoreCache[board]
	//searchCacheLock.RUnlock()
	if ok && int(cache.depth()) >= depth {
		searchHit[depth]++
		return int(cache.score())
	}
	searchMiss[depth]++

	var emptyPos [16]uint
	numCells := emptyCellsInPlace(board, &emptyPos)
	/*
	if numCells <= 1 && !extSearch {
		extSearch = true
		depth += 4
	}
	*/
	total := 0
	for i := 0; i < numCells; i++ {
		score := alphaAveragePlayer(placePiecePacked(board, emptyPos[i], 1), depth-1, steps+1, extSearch)
		//if score < LOSE / 2 {
		//	return score
		//}
		total += score
		score = alphaAveragePlayer(placePiecePacked(board, emptyPos[i], 2), depth-1, steps+1, extSearch)
		//if score < LOSE / 2 {
		//	return score
		//}
		total += score
	}
	score := total / (numCells * 2)
	/*
	min := math.MaxInt32
	for i := 0; i < numCells; i++ {
		score := alphaAveragePlayer(placePiecePacked(board, emptyPos[i], 1), depth-1, steps+1, extSearch)
		if score < min {
			min = score
		}
		score = alphaAveragePlayer(placePiecePacked(board, emptyPos[i], 2), depth-1, steps+1, extSearch)
		if score < min {
			min = score
		}
	}
	score := min
	*/

//	cells := emptyCells(board)
//	total := 0
	//out := make(chan int)
//	for _, pos := range cells {
		//total += alphaAveragePlayer(placePiece(board, pos, 2), depth-1, steps+1)
		//total += alphaAveragePlayer(placePiece(board, pos, 4), depth-1, steps+1)
//		total += alphaAveragePlayer(placePiecePacked(board, pos, 1), depth-1, steps+1)
//		total += alphaAveragePlayer(placePiecePacked(board, pos, 2), depth-1, steps+1)
		/*
		go func() {
			out <- alphaAveragePlayer(placePiece(board, pos, 2), depth-1, steps+1)
		}()
		go func() {
			out <- alphaAveragePlayer(placePiece(board, pos, 4), depth-1, steps+1)
		}()
		total += <-out
		total += <-out
		*/
//	}
//	score := total / (len(cells) * 2)
	//searchCacheLock.Lock()
	searchScoreCache[board] = makeSearchScore(depth, score)
	//searchCacheLock.Unlock()
	return score
}

func searchBestMove(board Board, nextBoards []Board, searchDepth int,  historyHighScore int) (int, int) {
	maxScore, maxMove := LOSE, 0
	empties := numEmptyCellsFast(board)
	if empties < 4 {
		searchDepth += 2
		/*
		if empties <= 1 {
			searchDepth += 2
		}
		*/
	}
	for retry := 0; retry <= 2; retry++ {
		if !*quiet && retry > 0 {
			fmt.Fprintf(os.Stderr, "deepening search %d\n", searchDepth)
		}
		for i, nextBoard := range nextBoards {
			score := alphaAverageObstacle(nextBoard, searchDepth, 1, false)
			if score > maxScore {
				maxScore = score
				maxMove = i
			}
		}
		if maxScore > historyHighScore - 50000 || empties > 5 {
			break
		}
		searchDepth += 2
	}
	return maxMove, maxScore
}

func runGame() {
	board := createStartBoard()
	highestScore := evaluateBoard(board)
	var steps uint
	searchDepth := 6
	if !*quiet {
		fmt.Println("I")
		fmt.Printf("%v", board)
	}
	for moves, nextBoards := availMoves(board); len(nextBoards) > 0; {
		var best int
		best, boardScore := searchBestMove(board, nextBoards, searchDepth, highestScore)
		if boardScore > highestScore {
			highestScore = boardScore
		}
		board = placeRandom24(nextBoards[best])
		if !*quiet {
			fmt.Fprintf(os.Stderr, "%d\n", boardScore)
			fmt.Printf("%v\n%v", moves[best], board)
		}
		moves, nextBoards = availMoves(board)
		steps++
		if steps % MAX_CACHE_STEPS == 0 {
			resetCacheTables()
		} /*else if steps == 500 {
			searchDepth += 2
		}*/
	}
	if *quiet {
		// print final board in quiet mode
		fmt.Printf("%v", board)
	} else {
		fmt.Println("E")
	}
}
