package main

import (
	"flag"
	"os"
	"reflect"
	"testing"
)

func TestMain(m *testing.M) {
	buildLookupTables()
	flag.Parse()
	os.Exit(m.Run())
}

func TestEvaluatePair(t *testing.T) {
	if evaluatePair(0, 0) != EMPTY_VALUE {
		t.Error("wrong pair weight for 0")
	}

	if evaluatePair(512, 256) != 1910 {
		t.Error("wrong pair weight for a/2 == b")
	}

	if evaluatePair(128, 128) != 401 {
		t.Error("wrong pair weight for a == b")
	}

	if evaluatePair(64, 128) > -1500 {
		t.Error("wrong pair weight for a < b")
	}

	if evaluatePair(64, 1024) > -30000 {
		t.Error("wrong pair weight for a < b")
	}
}

func TestEvaluateRow(t *testing.T) {
	if evaluateRow(0x0001) != 386 {
		t.Error("wrong row weight for (0 0 0 2)")
	}
	if evaluateRow(0x1234) != -42 {
		t.Error("wrong row weight for (2 4 8 16)")
	}
	if evaluateRow(0x4321) != 53 {
		t.Error("wrong row weight for (16 8 4 2)")
	}
	if evaluateRow(0xb0a1) != 13635 {
		t.Error("wrong row weight for (2048 0 1024 2)")
	}
}

func TestLookupTables(t *testing.T) {
	if rowScoreTable[0x0001] != 386 {
		t.Error("wrong row weight for (0 0 0 2)")
	}
	if rowScoreTable[0x1234] != -42 {
		t.Error("wrong row weight for (2 4 8 16)")
	}
	if moveRowLeftTable[0x1108] != moveRowLeft(0x1108) {
		t.Error("wrong move row left lookup result")
	}
	if moveRowRightTable[0x1108] != moveRowRight(0x1108) {
		t.Error("wrong move row right lookup result")
	}
}

func TestTranspose(t *testing.T) {
	if !reflect.DeepEqual(transpose([]uint{0x1234, 0x5678, 0x9abc, 0xdef0}), []uint{0x159d, 0x26ae, 0x37bf, 0x48c0}) {
		t.Error("wrong transpose result")
	}

	rows := [4]uint{0x1234, 0x5678, 0x9abc, 0xdef0}
    out := transposeInPlace(rows)
	if !reflect.DeepEqual(out, [4]uint{0x159d, 0x26ae, 0x37bf, 0x48c0}) {
		t.Errorf("wrong transposeInPlace result %v", rows)
	}
}

func TestTransposePacked(t *testing.T) {
	if transposePacked(0x123456789abcdef0) != 0x159d26ae37bf48c0 {
		t.Error("wrong transposePacked result")
	}
}

func TestEvaluateBoard(t *testing.T) {
	if evaluateBoard(0x123456789abcdef0) > -5000000 {
		t.Error("wrong evaluateBoard result")
	}
}

func TestMoveRowLeft(t *testing.T) {
	if moveRowLeft(0x1111) != 0x2110 {
		t.Errorf("move result for 0x1111: 0x%x, expected 0x2110", moveRowLeft(0x1111))
	}

	if moveRowLeft(0x2200) != 0x3000 {
		t.Errorf("move result for 0x2200: 0x%x, expected 0x3000", moveRowLeft(0x2200))
	}

	if moveRowLeft(0x0022) != 0x3000 {
		t.Errorf("move result for 0x0022: 0x%x, expected 0x3000", moveRowLeft(0x0022))
	}

	if moveRowLeft(0xa00b) != 0xab00 {
		t.Errorf("move result for 0xa00b: 0x%x, expected 0xab00", moveRowLeft(0xa00b))
	}

	if moveRowLeft(0x0a0a) != 0xb000 {
		t.Errorf("move result for 0x0a0a: 0x%x, expected 0xb000", moveRowLeft(0x0a0a))
	}
}

func TestMoveRowRight(t *testing.T) {
	res := moveRowRight(0x1111)
	if res != 0x0112 {
		t.Errorf("move result for 0x1111: 0x%x, expected 0x0112", res)
	}

	res = moveRowRight(0x2200)
	if res != 0x0003 {
		t.Errorf("move result for 0x2200: 0x%x, expected 0x0003", res)
	}

	res = moveRowRight(0xa00b)
	if res != 0x00ab {
		t.Errorf("move result for 0xa00b: 0x%x, expected 0x00ab", res)
	}

	res = moveRowRight(0x0a0a)
	if res != 0x000b {
		t.Errorf("move result for 0x0a0a: 0x%x, expected 0x000b", res)
	}
}

func TestMoveBoard(t *testing.T) {
	res := moveBoard(Board(0x0010201304002400), UP)
	if res != 0x3523000000000000 {
		t.Errorf("move result for board 0x0010201304002400: 0x%x, expected 0x3523000000000000", res)
	}

	res = moveBoard(Board(0x0010201304002400), LEFT)
	if res != 0x1000213040002400 {
		t.Errorf("move result for board 0x0010201304002400: 0x%x, expected 0x1000213040002400", res)
	}

	res = moveBoard(Board(0x0010201304002400), RIGHT)
	if res != 0x1021300040024 {
		t.Errorf("move result for board 0x0010201304002400: 0x%x, expected 0x0001021300040024", res)
	}

	res = moveBoard(Board(0x0010201304002400), DOWN)
	if res != 0x3523 {
		t.Errorf("move result for board 0x0010201304002400: 0x%x, expected 0x0000000000003523", res)
	}
}

func TestPlacePiece(t *testing.T) {
	res := placePiece(0x0010201304002400, 0, 64)
	if res != 0x6010201304002400 {
		t.Errorf("placePiece result: 0x%x, expected 0x6010201304002400", res)
	}

	res = placePiece(0x0010201304002400, 3, 2)
	if res != 0x0011201304002400 {
		t.Errorf("placePiece result: 0x%x, expected 0x0011201304002400", res)
	}

	res = placePiece(0x0010201304002400, 15, 32768)
	if res != 0x001020130400240f {
		t.Errorf("placePiece result: 0x%x, expected 0x001020130400240f", res)
	}
}

func TestPlacePiecePacked(t *testing.T) {
	res := placePiecePacked(0x0010201304002400, 0, 6)
	if res != 0x6010201304002400 {
		t.Errorf("placePiecePacked result: 0x%x, expected 0x6010201304002400", res)
	}
}

func TestEmptyCells(t *testing.T) {
	res := emptyCells(0x0010201304002400)
	if !reflect.DeepEqual(res, []uint{0, 1, 3, 5, 8, 10, 11, 14, 15}) {
		t.Errorf("emptyCells result %v", res)
	}
}

func TestNumEmptyCells(t *testing.T) {
	res := numEmptyCells(0x0010201304002400)
	if res != 9 {
		t.Errorf("bad numEmptyCells result %d", res)
	}

	res = numEmptyCellsFast(0x0010201304002400)
	if res != 9 {
		t.Errorf("bad numEmptyCellsFast result %d", res)
	}

	res = numEmptyCellsFast(0xab102c1304702400)
	if res != 5 {
		t.Errorf("bad numEmptyCellsFast result %d", res)
	}

	res = numEmptyCellsFast(0xab112c132478240d)
	if res != 1 {
		t.Errorf("bad numEmptyCellsFast result %d", res)
	}

	res = numEmptyCellsFast(0xab112c132478246d)
	if res != 0 {
		t.Errorf("bad numEmptyCellsFast result %d", res)
	}
}

func TestPlaceRandom24(t *testing.T) {
	res := placeRandom24(0x0010201304002400)
	if res == 0x0010201304002400 {
		t.Error("placeRandom24 generates the same board")
	}
	t.Logf("placeRandom24 result1 %v", res)
	t.Logf("placeRandom24 result2 %v", placeRandom24(0x0010201304002400))
	t.Logf("placeRandom24 result3 %v", placeRandom24(0x0010201304002400))
}

func TestCreateStartBoard(t *testing.T) {
	res := createStartBoard()
	if res == 0 {
		t.Error("initRandomBoard returned empty board")
	}
	t.Logf("initRandomBoard result1 %v", res)
	t.Logf("initRandomBoard result2 %v", createStartBoard())
	t.Logf("initRandomBoard result3 %v", createStartBoard())
}

func TestAvailMoves(t *testing.T) {
	res, boards := availMoves(0x0010201304002400)
	if !reflect.DeepEqual(res, allDirs[:]) {
		t.Errorf("availMoves %v, expected %v", res, allDirs)
	}
	for i, move := range res {
		if moveBoard(0x0010201304002400, move) != boards[i] {
			t.Error("new boards returned by availMoves don't match")
		}
	}

	board2 := moveBoard(0x0010201304002400, UP)
	res, _ = availMoves(board2)
	expected := []Dir{DOWN}
	if !reflect.DeepEqual(res, expected) {
		t.Errorf("availMoves %v, expected %v", res, expected)
	}

	board3 := moveBoard(0x0010201304002400, LEFT)
	res, _ = availMoves(board3)
	expected = []Dir{UP, RIGHT, DOWN}
	if !reflect.DeepEqual(res, expected) {
		t.Errorf("availMoves %v, expected %v", res, expected)
	}

	board4 := moveBoard(0x0010201304002400, RIGHT)
	res, _ = availMoves(board4)
	expected = []Dir{UP, LEFT, DOWN}
	if !reflect.DeepEqual(res, expected) {
		t.Errorf("availMoves %v, expected %v", res, expected)
	}
}

func TestAlphaAveragePlayer(t *testing.T) {
	res := alphaAveragePlayer(0x0010201304002400, 1, 0)
	if res != 4014 {
		t.Errorf("alphaAveragePlayer %d, expectd 4014", res)
	}

	res = alphaAveragePlayer(0x0010201304002400, 5, 0)
	if res != 3733 {
		t.Errorf("alphaAveragePlayer %d, expectd 3733", res)
	}
}

func TestAlphaAverageObstacle(t *testing.T) {
	res := alphaAverageObstacle(0x0010201304002400, 1, 0)
	if res != 2711 {
		t.Errorf("alphaAveragePlayer %d, expectd 2711", res)
	}

	res = alphaAverageObstacle(0x0010201304002400, 5, 0)
	// with score cache we expect 3274 instead of 3248
	if res != 3274 {
		t.Errorf("alphaAveragePlayer %d, expectd 3274", res)
	}
	/*
	   if res != 3248 {
	       t.Errorf("alphaAveragePlayer %d, expectd 3248", res)
	   }
	*/
}

func TestSearchScore(t *testing.T) {
    ss := makeSearchScore(5, 100000)
    if ss.depth() != 5 || ss.score() != 100000 {
        t.Errorf("Bad SearchScore")
    }
    ss = makeSearchScore(5, -100000)
    if ss.depth() != 5 || ss.score() != -100000 {
        t.Errorf("Bad SearchScore")
    }
}

