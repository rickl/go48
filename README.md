# go48: Yet Another 2048 Solver in Go

A 2048 solver written in golang.

## Build

Please see/use Dockerfile.

## Run

Please see/use Dockerfile.

Solving steps are printed to stdout, while debug logs to stderr.

## Args

  * `-q` : quiet mode. print only the final result.
  * `-s` : specify random seed. 0 means using system current timestamp (default).
  * `-d` : enable additional debug logs.
  * `-sw` : start with a specified number on board.
  * `-prof` : enable profiling.

### Bugs

...

## License

Copyright © 2015-2016 Rick Lei

Distributed under Apache License 2.0.

